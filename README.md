# TrueIDC CD (GitOps)

## Architecture

![Architecture](images/tcd.png)

---

## Preparation

1. Create [Gitlab Personal Access Token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#create-a-personal-access-token) with scope **`write_repository`** for access repository.

2. Add token to file `.secret.env`

    ```env
    GITLAB_TOKEN=<your_access_token>
    ```

3. As **Developer** clone **application** repository to local directory **`vote-src`**

    Open new terminal session

    ```sh
    source .secret.env

    git clone --branch develop https://oauth2:$GITLAB_TOKEN@gitlab.com/cloudpipeline/demo/apps/vote.git vote-src
    ```

4. As **DevOps** team clone **GitOps** repository to local directory **`vote-gitops`**

    Open new terminal session

    ```sh
    source .secret.env

    git clone --branch develop https://oauth2:$GITLAB_TOKEN@gitlab.com/cloudpipeline/demo/environments/vote-app.git vote-gitops
    ```

5. Reset demo

    - Reset gitops repo to master

        ```sh
        cd vote-gitops

        git checkout develop
        git reset --hard origin/master
        git push -f
        ```

    - Reset app repo to master

        ```sh
        cd vote-src

        git checkout develop
        git reset --hard origin/master
        git push -f
        ```

    - Delete image repository `sandbox/vote` on TCR

      ![Delete image](images/delete-sandbox-vote.png)

    - Delete `ApplicationSet` on `tcd` cluster.

      **Note** file `tcd.yaml` will be sent via email.

        ```sh
        kubectl --kubeconfig tcd.yaml delete appset --all
        ```

    - Delete namespace `vote-test` and `vote-prod` on `demo1` cluster

        **Noted** Download demo1's kubeconfig from TKS web console then rename to `demo1.yaml`

        ```sh
        kubectl --kubeconfig demo1.yaml delete ns vote-test vote-prod
        ```

---

## Scenario 1: Developer build image `v1.0.0`

```sh
cd vote-src

git tag -af v1.0.0 -m "First release"
git push origin -f v1.0.0
```

---

## Scenario 2: DevOps team setup GitOps on TCD via API

Setup TCD GitOps with these parameters:

- `repoUrl`  GitOps repository URL.
- `revision` Git branch for sync.

```sh
source .secret.env

curl -sS -X POST $TCD_API_URL \
  -H "Authorization: Bearer $TCD_API_TOKEN" \
  -H "Content-Type: application/json" \
  -d '{
      "resourceKind": "WorkflowTemplate",
      "resourceName": "setup-gitops",
      "submitOptions": {
        "parameters": [
          "repoUrl=https://gitlab.com/cloudpipeline/demo/environments/vote-app.git",
          "revision=develop"
        ]
      }
  }'
```

At this point, application will be rollout to `test` environment.

[https://vote-test.8wy74cyf3p65.th.tks.trueidc.com](https://vote-test.8wy74cyf3p65.th.tks.trueidc.com)

---

## Scenario 3: Developer modify application then automatic rollout

![CI Pipeline](images/ci.png)

![v1.0.0](images/test-v1.0.0.png)

- [https://vote-test.8wy74cyf3p65.th.tks.trueidc.com](https://vote-test.8wy74cyf3p65.th.tks.trueidc.com)
- [https://result-test.8wy74cyf3p65.th.tks.trueidc.com](https://result-test.8wy74cyf3p65.th.tks.trueidc.com)

**Developer** Modify application and push git tag `v1.0.1` to trigger `Gitlab CI` to publish image to **True Container Registry**

- Edit `vote-src/templates/index.html` change `1.0.0` to `1.0.1`

  ```html
  <div id="hostname">
    (1.0.1)Processed by container ID {{hostname}}
  </div>
  ```

- Commit changes and push tag `v1.0.1`. CI Pipeline will started.

  ```sh
  cd vote-src

  git commit -a -m "Update index.html"
  git tag -af v1.0.1 -m 'Update version in index.html'
  git push origin -f v1.0.1
  ```

- Open [Gitlab CI Pipeline](https://gitlab.com/cloudpipeline/demo/apps/vote/-/pipelines) for see build detail.

- Open [True Container Registry](https://tcr.tks.trueidc.com/harbor/projects/3/repositories/vote) for checking image `tcr.tks.trueidc.com/sandbox/vote:1.0.1` already pushed.

- Open [https://vote-test.8wy74cyf3p65.th.tks.trueidc.com](https://vote-test.8wy74cyf3p65.th.tks.trueidc.com). Page content will be changed to `1.0.1`

  ![test-v1.0.1](images/test-v1.0.1.png)

---

## Scenarion 4: DevOps team rollout new `prod` environment

![CD process](images/cd.png)

- Create new application and environment configuration files

  - `vote-gitops/app/values-prod.yaml`
  - `vote-gitops/env/prod/config.yaml`

  ```sh
  cd vote-gitops

  # Application configuration
  cat <<EOF > app/values-prod.yaml
  choices:
    a: Biden
    b: Trumb
  vote:
    replicas: 2
  EOF

  # Environment configuration
  mkdir -p env/prod
  cat <<EOF > env/prod/config.yaml
  images: |
    vote=tcr.tks.trueidc.com/sandbox/vote:1.x,
    result=tcr.tks.trueidc.com/sandbox/result:1.x,
    worker=tcr.tks.trueidc.com/sandbox/worker:1.x
  cluster:
    name: demo1
    namespace: vote-prod
  EOF
  ```

- Commit and push to Gitlab.

    ```sh
    git add .
    git commit -a -m "Rollout prod environment"
    git push -f
    ```

- TrueIDC CD will be synchronized GitOps repository then rollout `prod` to TKS with version `1.0.1`

Application URL will be available at:

- [https://vote-prod.8wy74cyf3p65.th.tks.trueidc.com](https://vote-prod.8wy74cyf3p65.th.tks.trueidc.com)
- [https://result-prod.8wy74cyf3p65.th.tks.trueidc.com](https://result-prod.8wy74cyf3p65.th.tks.trueidc.com)

![prod-v1.0.1](images/prod-v1.0.1.png)

---
